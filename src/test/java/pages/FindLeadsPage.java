package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}

	public FindLeadsPage clickPhonetab()
	{
		WebElement elePhonetab = locateElement("xpath", "//span[text()='Phone']");
		click(elePhonetab);
		return this;
	}
	public FindLeadsPage typePhoneNum(String data)
	{
		WebElement elePhoneNum = locateElement("xpath", "//input[@name='phoneNumber']");
		type(elePhoneNum, data);
		return this;
	}
	
	public FindLeadsPage clickEmailTab()
	{
		WebElement eleEmailtab = locateElement("xpath", "//span[text()='Email']");
		click(eleEmailtab);
		return this;
	}
	
	public FindLeadsPage typeEmail(String data)
	{
		WebElement eleEmail = locateElement("name", "emailAddress");
		type(eleEmail, data);
		return this;
	}
	
	public FindLeadsPage clickFindleads() throws InterruptedException
	{
		WebElement eleFindleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindleads);
		Thread.sleep(2000);
		return this;
	}
	
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleLeadID;
	public ViewLeadPage clickLeadId()
	{
		//WebElement eleLeadID = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleLeadID);
		return new ViewLeadPage();
	}
	
	public FindLeadsPage typeLeadId()
	{
		WebElement eletypeId = locateElement("name", "id");
		String id = eleLeadID.getText();
		type(eletypeId, id);
		return this;
	}
	
	public FindLeadsPage verifyErrorMessage(String data)
	{
		WebElement eleerrormess = locateElement("class", "x-paging-info");
		verifyExactText(eleerrormess, data);
		return this;
	}

}
