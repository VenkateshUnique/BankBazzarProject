package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(features = "src/test/java/requirementFeatures/CreateLead2.feature",
		glue = {"stepDefinitions","hooks"}, monochrome=true)
@RunWith(Cucumber.class)
public class testrun 
{

}
